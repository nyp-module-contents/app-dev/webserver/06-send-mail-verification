from data.base import Table, Row

class Product(Row):
	def __init__(self):
		super().__init__()
		self.__name        = ""
		self.__description = ""
		self.__price       = 0
		self.__image       = None

	@property
	def name(self) -> str:
		return self.__name

	@property
	def description(self) -> str:
		return self.__description

	@property
	def price(self) -> int:
		return self.__price

	@name.setter
	def name(self, value: str):
		self.__name = value

	@description.setter
	def description(self, value: str):
		self.__description = value
  
	@price.setter
	def price(self, value: int):
		self.__price = value
  
	@property
	def image(self) -> str:
		return self.__image

	@image.setter
	def image(self, value: str):
		self.__image = value

	def to_json(self) -> dict:
		return dict(super().to_json(), 
              name        = self.name,
              description = self.description,
              price       = self.price,
              image       = self.image)


class Products(Table[Product]):
	pass