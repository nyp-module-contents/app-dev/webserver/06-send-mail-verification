from   hashlib import sha256
import enum

from data.base            import Table, Row
from data.base.conditions import ConditionEQ

# User ENUM for CONSTANTS or Things that 4ever don't change
@enum.unique
class UserRole(enum.IntEnum):
	"""
	All the roles available in this system
	"""
	ADMIN = 0
	USER  = 1


class User(Row):
	"""
	A single instance of user in the system
	"""
	def __init__(self, role: UserRole, name: str, password: str, uid: str = None):
		super().__init__(uid)
		self.__role        = role
		self.__name        = name
		
		self.set_password(password)
		#   TODO:	Add your own things
  
	@property
	def role(self) -> UserRole:
		return self.__role

	@property
	def name(self) -> str:
		return self.__name

	@property
	def password(self) -> str:
		return self.__password

	@name.setter
	def name(self, value: str) -> str:
		self.__name = value

	def set_password(self, value: str) -> str:
		"""
		Set password

		Parameters
		----------
			value: str
				Plain text
		"""
		self.__password = sha256(value.encode('utf8')).hexdigest()

	def to_json(self) -> dict:
		return dict(super().to_json(), name=self.name, role=self.role)


class Users(Table[User]):
	"""
	Table containing Users.
	"""	

	@classmethod
	def login(cls, username: str, password: str) -> User:
		"""
		Attempts to login with the given credentials.

		Parameters
		----------
			username: str
				The user identifier
			password: str
				The plain text password
	
		Returns
		-------
			The target user or None if not found
		"""
		hash   = sha256(password.encode('utf8')).hexdigest()
		result = cls.select(None, ConditionEQ("name", username), ConditionEQ("password", hash))
		return result[0] if len(result) == 1 else None