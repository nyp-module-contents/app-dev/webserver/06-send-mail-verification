import logging
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

def send_mail(logger: logging.Logger, to: str, subject: str, content: str) -> bool:
    """
    Sends a email using sendgrid.
    Args:
        to: str
            To who
        subject: str
            Subject of email
        content: str
            HTML Content
    Returns:
        True if okay else False
    """
    client = SendGridAPIClient("YourOwnApiKey")

    try:
        response = client.send(Mail(
            to_emails   = to,
            from_email  = "<YourOwnEmailVerifiedinSendGrid>",
            html_content= content,
            subject     = subject
        ))
        logger.info(f"Successfully sent email to {to}")
        logger.info(f"Response: {response}")
        # logger.info(f"{content}")
        return True
    except Exception as e:
        logger.error(f"Failed to send email to {to}")
        logger.error(f"Reason {e}")
        return False