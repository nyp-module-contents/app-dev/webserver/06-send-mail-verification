"""
The routes module contains standalone examples
"""
import flask
from data.base.conditions import ConditionAny, ConditionEQ
import utils.flask
import json

#	This is a child route, so we don't specify url_prefix like the previous one
from data.products import Product, Products

router = flask.Blueprint("products", __name__)

@router.get("/")
def page_root():
	return flask.redirect(flask.url_for(".page_list"))
@router.get("/list")
def page_list():
	return flask.render_template("admin/products/list.html")
@router.get("/create")
def page_create():
	return flask.render_template("admin/products/create.html", data= {})

@router.get("/update/<id>")
def page_update(id:str):
	p = Products.retrieve(None, id)
	return flask.render_template("admin/products/update.html", id=id, data=p)

@router.get("/retrieve")
def api_retrieve():

	offset  = int(flask.request.args.get("offset", "0"))
	limit   = int(flask.request.args.get("limit",  "0"))
	sort    = flask.request.args.get("sort",  "name")
	order   = flask.request.args.get("order", "asc") == "asc"
	search  = flask.request.args.get("search", None)
	filters = json.loads(flask.request.args.get("filter", "{}"))
 
	conditions = []
	for (k, v) in filters.items():
		conditions.append(ConditionEQ(k, v))
	
	if search is not None and len(search) > 0:
		conditions.append(ConditionAny(search, "name", "description"))
 
	results    = Products.select(None, *conditions or [])
 
	utils.flask.logger().info(f"results: {len(results)} {len(conditions)}")
	
	if sort is not None:
		results = sorted(results, key=lambda x: getattr(x, sort), reverse= not order)

	return flask.jsonify({
		"total": len(results),
		"rows" : list(map(lambda x: x.to_json(), results[offset: offset + limit]))
	})

@router.post("/create")
def api_create():
	logger = utils.flask.logger()
	# for k in flask.request.form.keys():
	# 	logger.info(f"Key: {k} Value: {flask.request.form.get(k, 'null')}")
	p             = Product()
	p.name        = flask.request.form.get("name")
	p.price       = flask.request.form.get("price")
	p.description = flask.request.form.get("description")

	if "image" in flask.request.files:
		img = flask.request.files.get("image")

		#	/cdn/products/<image>
		import os
		import os.path
		from   uuid import uuid4
		id      = str(uuid4())
		path    = f"cdn/products/{id}"
		os.makedirs(os.path.join(os.getcwd(), os.path.dirname(path)), exist_ok=True)
		img.save(os.path.join(os.getcwd(), path))
		p.image = f"/cdn/products/{id}"

	# else:
	# 	return flask.abort(400)
	Products.insert(None, p)
	return flask.redirect(flask.url_for(".page_root"))

@router.post("/update/<id>")
def api_update(id: str):
	logger = utils.flask.logger()
	for k in flask.request.form.keys():
		logger.info(f"Key: {k} Value: {flask.request.form.get(k, 'null')}")
	data = flask.request.form.to_dict()
	logger.debug(f"Incoming data: {data}")

	with Products.open() as handle:
		p             = Products.retrieve(handle, id)
		p.name        = flask.request.form.get("name")
		p.price       = int(flask.request.form.get("price"))
		p.description = flask.request.form.get("description")

		#	Check if user uploaded image
		#	Do we have old image, have then delete, replace

		if "image" in flask.request.files:
			img = flask.request.files.get("image")

			#	/cdn/products/<image>
			import os
			import os.path
			from uuid import uuid4
			id = str(uuid4())
			path = f"cdn/products/{id}"
			os.makedirs(os.path.join(os.getcwd(), os.path.dirname(path)), exist_ok=True)
			img.save(os.path.join(os.getcwd(), path))

			if p.image is not None:
				utils.flask.logger().warning(f"Deleting existing file {p.image}")
				if os.path.exists(f"{os.getcwd()}{p.image}"):
					os.unlink(f"{os.getcwd()}{p.image}")

			p.image = f"/cdn/products/{id}"

		#	Replacement
		Products.update(handle, p)

	return flask.render_template("admin/products/update.html", id=id, data=data)


@router.get("/delete/<id>")
def api_delete(id: str):
	logger = utils.flask.logger()
	logger.warning(f"Deleted {id}")
	p = Products.remove(None, id)

	if p.image is not None:
		import os
		import os.path
		if os.path.exists(f"{os.getcwd()}{p.image}"):
			os.unlink(f"{os.getcwd()}{p.image}")
	return flask.redirect(flask.url_for(".page_root"))