"""
The routes module contains standalone examples
"""
import flask
import jwt

import utils.mail
import utils.flask
#	This is a child route, so we don't specify url_prefix like the previous one
router = flask.Blueprint("examples", __name__)

@router.get("/")
def page_home():
	return flask.render_template("./examples/home.html")

# >>> import jwt
# >>> encoded_jwt = jwt.encode({"some": "payload"}, "secret", algorithm="HS256")
# >>> print(encoded_jwt)
# eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzb21lIjoicGF5bG9hZCJ9.Joh1R2dYzkRvDkqv3sygm5YyK8Gi4ShZqbhK2gxcs2U
# >>> jwt.decode(encoded_jwt, "secret", algorithms=["HS256"])
# {'some': 'payload'}
@router.get("/send-mail")
def api_sends_email():
	to   = flask.request.args.get("to")
	code = jwt.encode({"email": to, "fieldx": "Some stuff"}, "secret", algorithm="HS256")

	if utils.mail.send_mail(
		logger=utils.flask.logger(),
		to=to,
		subject="OTP Email",
		content=flask.render_template("email/email-otp.html", code=code)):
		return flask.redirect("/")
	else:
		return flask.abort(500)

@router.get("/verify-mail/<string:token>")
def api_verify_email(token: str):
	try:
		payload = jwt.decode(token, "secret", algorithms=["HS256"])
		return flask.jsonify(payload)
		#	TODO:
		#	After user clicks,
			#	update user status to verified etc
	except Exception as e:
		return flask.abort(400)

@router.get("/respond-string")
def response_string():
	return "This is a text being returned"

@router.get("/respond-html")
def response_html():
	return """
	<html>
		<body>
			<h1>Explanation</h1>
			<p>The returned string gets interpreted as HTML and rendered on the browser</p>
			<a href="#">This link does nothing</a>
		</body>
	</html>
	"""

@router.get("/respond-template")
def response_template():
	return flask.render_template("./examples/template-plain.html")

@router.get("/respond-template-substitution")
def response_template_substitution():
	"""
	You can subsitute things in your template by Key and Value
	in this case, 'content' is the key found in the template
	and the value is just my string (numbers also can)
	
	Exercise, Try other types and see the results
	"""
	return flask.render_template("./examples/template-plain.html",
				text_color = "red",
				content    = "<h5>This line is subsituted from python</h5>",
				condition  = False)

@router.get("/respond-template-loops")
def response_template_loops():
	"""
	The same as response_template_substitution but this time what if your value
	is a collection?

	This example shows how to iterate array and dictionary
	"""
	random_array    = [1, 2, 3, 4, 5, 6, 7, 8, 9 , 10]
	random_array_2d = [[1, 2, 3, 4, 5, 6, 7, 8, 9 , 10],[1, 2, 3, 4, 5, 6, 7, 8, 9 , 10],[1, 2, 3, 4, 5, 6, 7, 8, 9 , 10]]
	random_dict     = {
		"name": "GUY",
		"city": "FreeCity"
	}
	random_table   = {
		"numbers": [1, 2, 3 , 4],
		"text":    ["a", "b", "c", "d"]
	}
	
	return flask.render_template("./examples/template-loop.html", 
				list         = random_array,
				list2d       = random_array_2d,
				table        = random_dict,
				table_object = random_table)