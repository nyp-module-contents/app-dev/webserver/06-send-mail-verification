window.addEventListener("DOMContentLoaded", function (event) {
	console.log(`Initializing before media load`);
	configure_validations();
});

window.addEventListener("load", function (event) {
	console.log(`Initializing after full load`);
});

function configure_validations() {
	// Fetch all the forms we want to apply custom Bootstrap validation styles to
	const forms = document.getElementsByClassName('needs-validation');
	// Loop over them and prevent submission
	for (const form of forms) {
		form.addEventListener('submit', function(event) {
			if (form.checkValidity() === false) {
				event.preventDefault();
				event.stopPropagation();
			}
			form.classList.add('was-validated');
		}, false);
	}
	console.log(`Configured Validations`);
}