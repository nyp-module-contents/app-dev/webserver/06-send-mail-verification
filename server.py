# Import the flask module, better editor support
# or if you like some examples, 'from flask import Flask'
import flask
import logging
import os
# Import your URL routes
import routes


#	Create a instance of a Flask web application
server: flask.Flask    = flask.Flask(__name__, 
										#	Tells it where to get /js /css /icon etc files from this directory
										static_folder  ="./public",
										#	Tells it to serve those folders and files as /public. E.g http://localhost:5000/public/js/main.js
										static_url_path="/public", 
										#	Tells it where we put all our templates relative to this directory
										template_folder="./templates")

server.config["SECRET_KEY"] = "RandomStuff"

def configure_logger(app: flask.Flask) -> logging.Logger:
	"""
	Configures the logger to format messages with the line number.
	Optional steps but you can also log it to a file and use for debugging / audit etc
	Always good to have.
	"""
	#	Type hint specified as the editor fails to recognize app.logger as a variable (property)
	#	It will be treated as a function by most editors. At least VSCode doesn't do this properly.
	logger:logging.Logger = app.logger
	#	By default, logger level is set to logging.ERROR
	#	To show everything printed, we set it to logging.INFO
	logger.setLevel(logging.INFO)
	#	Basically refer to https://stackoverflow.com/questions/533048/how-to-log-source-file-name-and-line-number-in-python
	#	Example:
	#		[10-26 08:56:11][INFO] {D:\Modules\dev-full-stack-flask\01-barebone\server.py:43}
	#		>> Server initialzing
	logFmt = logging.Formatter("[%(asctime)s][%(levelname)s] {%(pathname)s:%(lineno)d}\n>> %(message)s",'%m-%d %H:%M:%S')
	#	The logger have one or more handlers, these handlers uses the formatter
	#	so we just set to every one of them...
	for handle in logger.handlers:
		handle.setFormatter(logFmt)
	#	Return for convenience
	return logger

@server.errorhandler(401)
def error_unauthorized(error):
    return flask.Response(
		response=flask.render_template("error.html", msg="You need to sign in to proceed!", link={
        "url": "/auth/login",
        "msg": "Login"
	}), status=401)

@server.errorhandler(403)
def error_forbidden(error):
    return flask.Response(
		response=flask.render_template("error.html", msg="You don't have access rights to this area. Or are you attempting something weird"),
		status=403)

@server.errorhandler(404)
def error_page_not_found(error):
    return flask.Response(
		response=flask.render_template("error.html", msg="Oops page not found. Are you trying something weird?"),
		status=404)

if (__name__ == "__main__"):
	"""
	Main program entry point
	"""
	logger = configure_logger(server)
	logger.info("Server initialzing")
	#	Configure your server here
	server.register_blueprint(routes.router)
	#	Once done, run your server
	#	This statement basically listens to the 'host' address and port 'forever' until quit.
	server.run(host="localhost", port=5000, debug=True)
	#	To tell you your server stopped running
	#	You can do any clean up here if any
	logger.info("Server terminated")
	#	Just to indicate end of code
	pass

